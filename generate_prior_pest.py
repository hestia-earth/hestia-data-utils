import argparse
import time
from dotenv import load_dotenv


load_dotenv()


from hestia_earth.distribution.prior_pest import generate_prior_pest_file


parser = argparse.ArgumentParser(description='Generating Pesticide Use Priors File')
parser.add_argument('--overwrite', action='store_true',
                    help='Whether to overwrite existing file')
args = parser.parse_args()


def main():
    start_time = time.time()
    generate_prior_pest_file(overwrite=args.overwrite)
    print(f"Finished. Total time: {int(time.time() - start_time)} secs.")


if __name__ == "__main__":
    main()
