### Usage
### pip install seaborn
import argparse
import numpy as np
import matplotlib.pyplot as plt
from dotenv import load_dotenv


load_dotenv()


from hestia_earth.distribution.cycle import (
    YIELD_COLUMN, FERTILISER_COLUMNS, PESTICIDE_COLUMN, IRRIGATION_COLUMN
)
from hestia_earth.distribution.utils.MCMC_mv import _fit_user_data_mv, _get_df_bounds
from hestia_earth.distribution.likelihood import generate_likl_file


parser = argparse.ArgumentParser()
parser.add_argument('--output', type=str, default='yield_vs_n.png',
                    help='Filepath to save the result to as PNG.')
parser.add_argument('--y', type=int, required=True,
                    help='Candidate grain yield value (kg/ha) to be tested.')
parser.add_argument('--n', type=int, default=None,
                    help='Candidate Nitrogen usage (kgN/ha) to be tested.')
parser.add_argument('--p', type=int, default=None,
                    help='Candidate Phosphorus usage (kgP2O5/ha) to be tested.')
parser.add_argument('--k', type=int, default=None,
                    help='Candidate Potassium usage (kgK2O/ha) to be tested.')
parser.add_argument('--pest', type=float, default=None,
                    help='Total pesticide usage (kg active ingredient / ha) to be tested.')
parser.add_argument('--water', type=float, default=None,
                    help='Total irrigation water usage (m3 / ha) to be tested.')
parser.add_argument('--country-id', type=str, required=True,
                    help='Region `@id` from Hestia glossary')
parser.add_argument('--product-id', type=str, required=True,
                    help='Product term `@id` from Hestia glossary')
parser.add_argument('--threshold', type=float, default=0.95,
                    help='Percentile threshold to be accepted')
args = parser.parse_args()


def main():
    if args.n is not None:
        var_name = args.n
        col_name = FERTILISER_COLUMNS[0]  # 'n'
    elif args.p is not None:
        var_name = args.p
        col_name = FERTILISER_COLUMNS[1]  # 'p'
    elif args.k is not None:
        var_name = args.k
        col_name = FERTILISER_COLUMNS[2]  # 'k'
    elif args.pest is not None:
        var_name = args.pest
        col_name = PESTICIDE_COLUMN  # 'pesticide'
    elif args.water is not None:
        var_name = args.water
        col_name = IRRIGATION_COLUMN  # 'water'

    plot_bivariate([var_name, args.y], args.country_id, args.product_id,
                   [col_name, YIELD_COLUMN],
                   args.output, args.threshold)


def plot_bivariate(candidate: list, country_id: str, product_id: str, columns: list,
                   output_file: str, threshold: float = 0.95):

    df = generate_likl_file(country_id, product_id)
    likelihood, Z = _fit_user_data_mv(candidate, df, columns, return_z=True)

    if likelihood != None:

        m, mins, maxs = _get_df_bounds(df, columns)

        # plot Z
        fig, ax = plt.subplots(figsize=(8, 8))
        im1 = ax.imshow(np.rot90(Z), cmap=plt.cm.gist_earth_r,
                        extent=[mins[0], maxs[0], mins[1], maxs[1]])
        ax.plot(m[0], m[1], 'k.', markersize=1)
        ax.set_xlim([mins[0], maxs[0]])
        ax.set_ylim([mins[1], maxs[1]])

        # plot candidate
        marker = 'ro' if likelihood < 1 - threshold else 'co'
        ax.plot(candidate[0], candidate[1], marker,
                label=f'Candidate {candidate} stands above {likelihood:.2%} of samples.')

        plt.gca().set_aspect('auto', adjustable='datalim')
        plt.legend()
        plt.colorbar(im1)
        plt.title(f"Where your data stand, 2D Distribution ({columns[1].split(' (')[0]} vs {columns[0].split('(')[0]})")
        plt.xlabel(columns[0])
        plt.ylabel(columns[1])

        plt.savefig(output_file, format='png')


if __name__ == "__main__":
    main()
