### Usage
### pip install seaborn
import argparse
import seaborn as sns
from dotenv import load_dotenv


load_dotenv()


from hestia_earth.distribution.utils.cycle import find_cycles
from hestia_earth.distribution.cycle import YIELD_COLUMN, FERTILISER_COLUMNS, cycle_yield_distribution


parser = argparse.ArgumentParser()
parser.add_argument('--output-file', type=str, default='yield.png',
                    help='Filepath to save the result to as PNG.')
parser.add_argument('--limit', type=int, default=100,
                    help='Set the sample limit. Defaults to 100.')
parser.add_argument('--country-id', type=str, required=True,
                    help='Region `@id` from Hestia glossary')
parser.add_argument('--product-id', type=str, required=True,
                    help='Product term `@id` from Hestia glossary')
args = parser.parse_args()


def main():
    cycles = find_cycles(args.country_id, args.product_id, limit=args.limit)

    sns.set()
    print('Computing yield distribution...')
    df_yield = cycle_yield_distribution(cycles)

    hue = 'Phosphorus used'
    df_yield[hue] = (df_yield['Phosphorus (kg P2O5)'] > 0)
    df_yield.loc[:, hue] = df_yield[hue].map({True: 'Yes', False: 'No or unknown'})  # Replace boolean by string
    hue_order = ['Yes', 'No or unknown']

    print('Plotting...')
    sns.jointplot(
        data=df_yield, x=FERTILISER_COLUMNS[0], y=YIELD_COLUMN, kind='kde', hue=hue, hue_order=hue_order
    ).savefig(args.output_file)


if __name__ == "__main__":
    main()
