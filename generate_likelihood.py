import argparse
import time
from dotenv import load_dotenv


load_dotenv()


from hestia_earth.distribution.likelihood import generate_likl_file


parser = argparse.ArgumentParser(description='Generating Yield Likelihood File')
parser.add_argument('--overwrite', action='store_true',
                    help='Whether to overwrite existing file')
parser.add_argument('--limit', type=int,
                    default=1000,
                    help='Limit of returned number of cycles')
parser.add_argument('--country-id', type=str, required=True,
                    help='Region `@id` from Hestia glossary')
parser.add_argument('--product-id', type=str, required=True,
                    help='Product term `@id` from Hestia glossary')
args = parser.parse_args()


def main():
    start_time = time.time()
    generate_likl_file(args.country_id, args.product_id, limit=args.limit, overwrite=args.overwrite)
    print(f"Finished. Total time: {int(time.time() - start_time)} secs.")


if __name__ == "__main__":
    main()
