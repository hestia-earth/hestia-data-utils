# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.0](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.15...v0.1.0) (2023-10-20)


### ⚠ BREAKING CHANGES

* **mcmc:** Requires pymc version `5.0.2`.

### Features

* **distribution:** replace bivariate with multivariate distribution ([ef655b0](https://gitlab.com/hestia-earth/hestia-distribution/commit/ef655b0c26f043cb0ad5bb32e7bed847fe9ae4fe)), closes [#48](https://gitlab.com/hestia-earth/hestia-distribution/issues/48)
* **mcmc:** multivariate posterior with yield and fertiliser inputs ([83b18ed](https://gitlab.com/hestia-earth/hestia-distribution/commit/83b18ed5547653d9ba6121da77ea0867ff998d5f))

### [0.0.15](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.14...v0.0.15) (2023-08-18)


### Features

* **likelihood:** restrict to cycles with functional unit of `1 ha` ([fe2f4a9](https://gitlab.com/hestia-earth/hestia-distribution/commit/fe2f4a98a8a1c4ea91852f6245186c773153b128))
* return sorted list of countries ([7b09617](https://gitlab.com/hestia-earth/hestia-distribution/commit/7b09617763feccc49380f2ce09507f4c010c1db5))
* **utils:** add bivariate plotting for pesticide and irrigation ([9edcbaf](https://gitlab.com/hestia-earth/hestia-distribution/commit/9edcbaf6ef46a7488f99d867b4696ac82c997e4f))
* **utils:** add bivariate probability using mcmc method ([2481bad](https://gitlab.com/hestia-earth/hestia-distribution/commit/2481bad2ae09d813f8a63d5d4ba2c3e825a176f5))
* **utils:** bivariate plotting for p and k fertilisers ([e82f564](https://gitlab.com/hestia-earth/hestia-distribution/commit/e82f564a47239e44888b984c0c7ac383dbb9c967))


### Bug Fixes

* **cycle:** sum yield of products with same term ([4692ffd](https://gitlab.com/hestia-earth/hestia-distribution/commit/4692ffd00a21eae41bb7ae41aa7cddcda5db1771)), closes [#45](https://gitlab.com/hestia-earth/hestia-distribution/issues/45)

### [0.0.14](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.13...v0.0.14) (2023-07-07)


### Bug Fixes

* **likelihood:** fix grouping value for fertilisers in `kg` ([a30245d](https://gitlab.com/hestia-earth/hestia-distribution/commit/a30245d2aa595bbf41bfe817cc12e3ff0101e212))
* **likelihood:** write to file even if no cycles found ([80e64cf](https://gitlab.com/hestia-earth/hestia-distribution/commit/80e64cffe4fac691064372e03a716996b668ae89))

### [0.0.13](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.12...v0.0.13) (2023-06-30)


### Bug Fixes

* **likelihood:** use zero for complete inputs but no stored value ([d10e7eb](https://gitlab.com/hestia-earth/hestia-distribution/commit/d10e7eb5137aa66653ef4e51a2dd2d1b48efd49b)), closes [#34](https://gitlab.com/hestia-earth/hestia-distribution/issues/34)
* **posterior:** handle no sigma value ([d0a277b](https://gitlab.com/hestia-earth/hestia-distribution/commit/d0a277bb19c392b54c15aaaf8a8cf1e03951b7e0))

### [0.0.12](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.11...v0.0.12) (2023-05-08)


### Features

* **posterior:** add irrigation ([3e00fa9](https://gitlab.com/hestia-earth/hestia-distribution/commit/3e00fa9a586ed2c4a650e32de73226de514eecb2))
* **posterior:** ignore cycles with incomplete products ([97547a9](https://gitlab.com/hestia-earth/hestia-distribution/commit/97547a94563f4e4d2790637eb1d7f9222d5d9611))

### [0.0.11](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.10...v0.0.11) (2023-05-01)


### Features

* **distribution:** add prior of irrigation ([bc5c5d2](https://gitlab.com/hestia-earth/hestia-distribution/commit/bc5c5d25b9750c14c599da9052f3ec299b022d63))
* **distribution:** building pesticide priors ([574dbb3](https://gitlab.com/hestia-earth/hestia-distribution/commit/574dbb3f310b7876ab7db3be35a0c63a92c9bd24))
* **distribution:** test pesticide priors ([571f1f9](https://gitlab.com/hestia-earth/hestia-distribution/commit/571f1f9c559e381c9eb12cec50d21f45d2caa99c))
* **distribution:** widen prior by using global variance ([69ee5e1](https://gitlab.com/hestia-earth/hestia-distribution/commit/69ee5e1a3889f9579a6579d7251ac983bf43ccc9))
* **posterior:** drop cycles where relevant data is incomplete ([eabc6e9](https://gitlab.com/hestia-earth/hestia-distribution/commit/eabc6e9319bdeb4ba7c8ca1c745f23b918293c51))
* **posterior:** handle pesticides ([ffed8b1](https://gitlab.com/hestia-earth/hestia-distribution/commit/ffed8b1f3f03c558821e0cdfae292458323b67c5))


### Bug Fixes

* **cycle:** default to nan when no value ([b873fa8](https://gitlab.com/hestia-earth/hestia-distribution/commit/b873fa8a1be98faa9b66ff553ebf37dbce505de3))
* **likelihood:** set `-` instead of `0` when no value ([a634871](https://gitlab.com/hestia-earth/hestia-distribution/commit/a634871ecba8639b6ed6739e017e94493a5abc68))
* **posterior:** add pymc line to update posterior ([a5a75d8](https://gitlab.com/hestia-earth/hestia-distribution/commit/a5a75d8a0fcc5977b975e416777875ba2758a810)), closes [#29](https://gitlab.com/hestia-earth/hestia-distribution/issues/29)
* **posterior:** handle series when dropping cycles ([0aebdc9](https://gitlab.com/hestia-earth/hestia-distribution/commit/0aebdc98288f947d4ad860584f0b63ea4d471623))
* **posteriors:** handle `np.nan` values ([9c887f3](https://gitlab.com/hestia-earth/hestia-distribution/commit/9c887f36a581a155699db09b21ae8afbef146dfc))
* **priors:** handle `np.nan` values ([cc010b4](https://gitlab.com/hestia-earth/hestia-distribution/commit/cc010b462afe8b4866160f7e93d706558b5570ca))

### [0.0.10](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.9...v0.0.10) (2023-02-13)

### [0.0.9](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.8...v0.0.9) (2023-02-10)


### Features

* **posterior fert:** get values by product and input ([ac3b0bf](https://gitlab.com/hestia-earth/hestia-distribution/commit/ac3b0bf935fe740975230b1f42d57fece774c466))


### Bug Fixes

* **fao:** use new lookup for fertilisers ([f5ca79b](https://gitlab.com/hestia-earth/hestia-distribution/commit/f5ca79b13481f15e289d1b2033258ca6ada4330b))

### [0.0.8](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.7...v0.0.8) (2023-01-28)


### Bug Fixes

* **cycle:** handle cycle with no primary product ([1ecff09](https://gitlab.com/hestia-earth/hestia-distribution/commit/1ecff09d8532c0f42249fefda0a968b1ec3d320d))
* **prior fertiliser:** use input `[@id](https://gitlab.com/id)` to get prior ([fe8ff7a](https://gitlab.com/hestia-earth/hestia-distribution/commit/fe8ff7aaff9aff480b2aac87ac52344b39668c4f))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.6...v0.0.7) (2023-01-26)


### Features

* **cycle:** add function to group fertiliser by inputs ([1bdcec0](https://gitlab.com/hestia-earth/hestia-distribution/commit/1bdcec051ca1199d81df6723e238161bd3460a2f))
* **posterior:** handle fertilisers ([81034f7](https://gitlab.com/hestia-earth/hestia-distribution/commit/81034f7321b61a2687a25eae44bae63e8a0464ab))


### Bug Fixes

* **prior:** scale sd by '3.0' ([d5f225c](https://gitlab.com/hestia-earth/hestia-distribution/commit/d5f225c74f42780cd3f41c77c4b43d49a299af17))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.5...v0.0.6) (2022-12-09)


### Features

* **prior fert:** add function to get priors for country/product ([6eb7ccb](https://gitlab.com/hestia-earth/hestia-distribution/commit/6eb7ccb6a39bfc53ae76e2e25a4f016382476aa4))
* **prior yield:** add function to get priors for country/product ([1401e07](https://gitlab.com/hestia-earth/hestia-distribution/commit/1401e0709c69129e267e492f9487832f3e342b6b))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.4...v0.0.5) (2022-12-07)


### Features

* **posterior:** add function to return ensemble instead of mean values ([1bd027c](https://gitlab.com/hestia-earth/hestia-distribution/commit/1bd027ccc8be64a5536f366a99fcef4c27b16d22))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.3...v0.0.4) (2022-12-06)


### Features

* **posterior:** save file for country/product even if no likelihood data ([5428ee1](https://gitlab.com/hestia-earth/hestia-distribution/commit/5428ee16e6681f79708a85ef8d74672f049d18e6))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.2...v0.0.3) (2022-12-05)


### Bug Fixes

* **prior:** scale sd by `2.5` ([a4ef7cf](https://gitlab.com/hestia-earth/hestia-distribution/commit/a4ef7cf2069b40c9912303d1670cf67a6e2643ff))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.1...v0.0.2) (2022-12-05)


### Features

* **likelihood:** save file even if empty ([24a76f1](https://gitlab.com/hestia-earth/hestia-distribution/commit/24a76f1c3eb8dcb3a0b071297f578616f97fbc11))

### [0.0.1](https://gitlab.com/hestia-earth/hestia-distribution/compare/v0.0.0...v0.0.1) (2022-12-05)


### Features

* **prior:** save file as CSV ([789515b](https://gitlab.com/hestia-earth/hestia-distribution/commit/789515be2709086dd469a566b098994380613d86))

## 0.0.0 (2022-12-01)


### Features

* **distribution:** building yield priors ([333d4da](https://gitlab.com/hestia-earth/hestia-distribution/commit/333d4da5f789a24829b67dd2f1a3f3825b48aeae))
* **distribution:** create distribution of Cycle yield by fertilizer used ([bb90f1c](https://gitlab.com/hestia-earth/hestia-distribution/commit/bb90f1c67350a259c65b444ecab36d971fb7a1a7))
* **distribution:** fertliser priors ([046a7ad](https://gitlab.com/hestia-earth/hestia-distribution/commit/046a7adb3f9c06253905c889e77228611f49fceb)), closes [#7](https://gitlab.com/hestia-earth/hestia-distribution/issues/7)
* **distribution:** fertliser priors ([ddbc511](https://gitlab.com/hestia-earth/hestia-distribution/commit/ddbc511627992d288ef21fcde56a58671f96f60f)), closes [#7](https://gitlab.com/hestia-earth/hestia-distribution/issues/7) [#6](https://gitlab.com/hestia-earth/hestia-distribution/issues/6)
* **distribution:** fertliser priors ([4e43b05](https://gitlab.com/hestia-earth/hestia-distribution/commit/4e43b058912e2215970da669e45d620b71f12feb)), closes [#7](https://gitlab.com/hestia-earth/hestia-distribution/issues/7)
* **distribution:** incorporate fertiliser completeness ([1535471](https://gitlab.com/hestia-earth/hestia-distribution/commit/15354716a04537c9899181ecf414e4ec18e928ee))
* **distribution:** remove likelihood file ([a6fa105](https://gitlab.com/hestia-earth/hestia-distribution/commit/a6fa105adad518f7d8115643804ee85b3efbf8c0))
* **distribution:** reorganise code ([9b216ef](https://gitlab.com/hestia-earth/hestia-distribution/commit/9b216ef43235523c22cc4f7e93eefe3ce5358e51))
* **log:** add logger ([fee56a4](https://gitlab.com/hestia-earth/hestia-distribution/commit/fee56a4906113fec8ce4cb81b223f49fbfbee669))
* **posterior:** build posterior files ([62b0626](https://gitlab.com/hestia-earth/hestia-distribution/commit/62b0626c3982a54999dea00ecfeb80fb67249bb2))


### Bug Fixes

* **cycle yield:** rename `fertilizer` to `fertiliser` ([05337b8](https://gitlab.com/hestia-earth/hestia-distribution/commit/05337b84270cbac1fedb9c0528977f011e0c6c5d)), closes [#11](https://gitlab.com/hestia-earth/hestia-distribution/issues/11)
* **distribution:** use product `mean` instead of `sum` ([cb2b5a7](https://gitlab.com/hestia-earth/hestia-distribution/commit/cb2b5a742cfa2b3f4dda26ed32728a29d7e82ccd))
* **plot cycle yield:** filter `aggregated` data ([3341492](https://gitlab.com/hestia-earth/hestia-distribution/commit/334149206d949bcdde676b49912f3533a698fd3c))
