import argparse
import time
from dotenv import load_dotenv


load_dotenv()


from hestia_earth.distribution.posterior_fert import update_all_post, get_post_ensemble


parser = argparse.ArgumentParser(description='Generating Fertiliser Posterior Files')
parser.add_argument('--overwrite', action='store_true',
                    help='Whether to overwrite existing file')
parser.add_argument('--country-id', type=str, required=True,
                    help='Region `@id` from Hestia glossary')
parser.add_argument('--product-id', type=str,
                    help='Term `@id` from Hestia glossary')
parser.add_argument('--input-id', type=str,
                    help='Term `@id` from Hestia glossary')
args = parser.parse_args()


def main():
    start_time = time.time()

    if args.product_id and args.input_id:
        get_post_ensemble(args.country_id, args.product_id, args.input_id, overwrite=args.overwrite)
    else:
        update_all_post(args.country_id, overwrite=args.overwrite)

    print(f"Finished. Total time: {int(time.time() - start_time)} secs.")


if __name__ == "__main__":
    main()
