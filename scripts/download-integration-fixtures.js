require('dotenv').config();

const { writeFileSync, readdirSync, unlinkSync, existsSync } = require('fs');
const { resolve, join } = require('path');
const axios = require('axios');

const [countryName, productName] = process.argv.slice(2);

const encoding = 'utf8';
const API_URL = process.env.API_URL || 'https://api.hestia.earth';
const ROOT = resolve(join(__dirname, '../'));
const fixturesFolder = join(ROOT, 'tests', 'fixtures', 'integration');

const searchCycles = async () => {
  const query = {
    bool: {
      must: [
        { match: { '@type': 'Cycle' } },
        {
          nested: {
            path: 'products',
            query: {
              bool: {
                must: [
                  { match: { 'products.term.name.keyword': productName } },
                  { match: { 'products.primary': true } }
                ]
              }
            }
          }
        },
        { match: { 'site.country.name.keyword': countryName } }
      ],
      must_not: [
        { match: { aggregated: true } }
      ]
    }
  };
  const { data: { results } } = await axios.post(`${API_URL}/search`, {
    limit: 1000,
    fields: ['@id'],
    query
  });
  return results;
};

const listExistingCycles = () =>
  readdirSync(join(fixturesFolder, 'cycles'))
    .filter(filename => filename.endsWith('.jsonld'))
    .map(filename => filename.split('.')[0])

const getData = async (url) => {
  const {
    data: { '@context': contextUrl, defaultSource, source, emissions, measurements, practices, originalId, ...data }
  } = await axios.get(url);
  return { ...data, dataPrivate: true };
};

const cycleProduct = (cycle) => cycle.products?.find(p => p.primary && p.term.name === productName);

const sum = (values) => values.reduce((p, c) => p + c, 0);

const downloadCycle = async (cycleId) => {
  const filepath = join(fixturesFolder, 'cycles', `${cycleId}.jsonld`);
  try {
    const cycle = await getData(`${API_URL}/cycles/${cycleId}`);
    if (!cycle.completeness?.products) {
      throw Error('Cycle products not complete');
    }
    // make sure primary product has a value
    if (!cycleProduct(cycle)?.value?.length) {
      throw Error('Cycle product has no value');
    }
    cycle.site.country = (await getData(`${API_URL}/sites/${cycle.site['@id']}`)).country;
    writeFileSync(filepath, JSON.stringify(cycle, null, 2), encoding);
    return cycle;
  }
  catch (err) {
    console.error('Could not download Cycle or Site, skipping', cycleId, err);
    try { unlinkSync(filepath) }
    catch (err1) {}
  }
  return null;
};

const generateCSV = (cycles) => {
  const filename = `posterior_${cycles[0].site.country['@id']}_${cycles[0].products?.find(p => p.primary).term['@id']}.csv`;
  const filepath = join(fixturesFolder, filename);
  // only generate if it does not exist
  if (!existsSync(filepath)) {
    console.log('Generating posterior CSV file...');
    const content = [
      [
        'cycle.id',
        'Grain yield (kg/ha)',' valid_yield',
        'Nitrogen (kg N)', 'Phosphorus (kg P2O5)', 'Potassium (kg K2O)', 'Magnesium (kg Mg)', 'completeness.fertiliser', 'valid_fert',
        'pesticideUnspecifiedAi', 'completeness.pesticidesAntibiotics', 'valid_pest',
        'waterSourceUnspecified', 'completeness.water', 'valid_water'
      ].join(','),
      ...(
        cycles.map(cycle => [
          cycle['@id'],
          sum(cycleProduct(cycle)?.value),
          '', '', '', '', cycle.completeness.fertiliser.toString().toUpperCase(), '',
          '', cycle.completeness.pesticidesAntibiotics.toString().toUpperCase(), '',
          '', cycle.completeness.water.toString().toUpperCase(), ''
        ].join(','))
      )
    ].join('\n');
    writeFileSync(filepath, content, encoding);
  }
};

const run = async () => {
  const cycleIds = countryName && productName
    ? (await searchCycles()).map(cycle => cycle['@id'])
    : listExistingCycles();

  console.log('Found', cycleIds.length, 'cycles for', productName, 'in', countryName);
  const cycles = await Promise.all(cycleIds.map(downloadCycle));
  return cycles?.length ? generateCSV(cycles.filter(Boolean)) : null;
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
