import os
import numpy as np
import PIL.Image as Image

fixtures_path = os.path.abspath('tests/fixtures')
COLLECTION = {'collection': 'collection', 'ee_type': 'ee_type'}


def get_results(response: dict): return list(map(lambda f: f.get('properties'), response.get('features')))


def get_result(response: dict, key: str): return get_results(response)[0].get(key)


def compare_images(image_1: str, image_2: str):
    img1 = Image.open(image_1)
    img2 = Image.open(image_2)
    img2 = img2.convert(img1.mode)
    img2 = img2.resize(img1.size)

    sum_sq_diff = np.sum((np.asarray(img1).astype('float') - np.asarray(img2).astype('float'))**2)
    normalized_sum_sq_diff = sum_sq_diff / np.sqrt(sum_sq_diff)
    return normalized_sum_sq_diff


def remove_file(fixtures_file: str):
    filepath = os.path.join(fixtures_path, fixtures_file)
    return os.remove(filepath) if os.path.exists(filepath) else None


def is_empty(value: str):
    return value == '-' or np.isnan(value)


def round_df_column(df, col):
    df[col] = df[col].apply(lambda x: int(round(x, 0)) if not is_empty(x) else x)
    return df


def fake_read_prior_file(folder: str):
    def read_file(*args):
        with open(os.path.join(fixtures_path, folder, 'result.csv'), 'rb') as f:
            return f.read()
    return read_file
