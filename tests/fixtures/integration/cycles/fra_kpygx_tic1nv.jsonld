{
  "@context": "https://www-staging.hestia.earth/schema/Cycle.jsonld",
  "createdAt": "2023-06-21",
  "@id": "fra_kpygx_tic1nv",
  "@type": "Cycle",
  "endDate": "2008",
  "numberOfCycles": 1,
  "functionalUnit": "1 ha",
  "description": "Farmer baker using animal labour, reduced tillage",
  "defaultMethodClassification": "verified survey data",
  "defaultMethodClassificationDescription": "Data collected through series of direct, semi-structured interviews with producers and further correspondence via post and e-mail",
  "completeness": {
    "fertiliser": true,
    "soilAmendments": true,
    "pesticidesAntibiotics": true,
    "water": false,
    "electricityFuel": false,
    "products": true,
    "cropResidue": false,
    "excreta": true,
    "other": true,
    "material": false,
    "animalFeed": true,
    "waste": true,
    "liveAnimals": false,
    "transport": false,
    "operations": false,
    "@type": "Completeness"
  },
  "defaultSource": {
    "@id": "gv7xxblrtiex",
    "@type": "Source"
  },
  "site": {
    "@id": "qrda5rhf0bil",
    "@type": "Site",
    "country": {
      "@type": "Term",
      "termType": "region",
      "name": "United Kingdom",
      "@id": "GADM-GBR"
    }
  },
  "products": [
    {
      "term": {
        "@type": "Term",
        "termType": "crop",
        "name": "Wheat, grain",
        "units": "kg",
        "@id": "wheatGrain"
      },
      "value": [
        1436.865021770682
      ],
      "variety": "landraces",
      "description": "Supplementary data, Production / Arable land",
      "transport": [
        {
          "term": {
            "@type": "Term",
            "termType": "transport",
            "name": "Freight, tractor and trailer",
            "units": "tkm",
            "@id": "freightTractorAndTrailer"
          },
          "value": 52.92,
          "returnLegIncluded": false,
          "@type": "Transport"
        }
      ],
      "primary": true,
      "@type": "Product"
    }
  ],
  "inputs": [
    {
      "term": {
        "@type": "Term",
        "name": "Compost (kg mass)",
        "termType": "organicFertiliser",
        "@id": "compostKgMass",
        "units": "kg"
      },
      "value": [
        12000
      ],
      "isAnimalFeed": false,
      "description": "Compost horse manure(72 kg N/ha, 30 kg P2O5/ha, 50 kg K2O/ha)",
      "@type": "Input"
    },
    {
      "term": {
        "@type": "Term",
        "name": "Pesticide unspecified (AI)",
        "termType": "pesticideAI",
        "@id": "pesticideUnspecifiedAi",
        "units": "kg active ingredient"
      },
      "value": [
        0
      ],
      "description": "no pesticide",
      "@type": "Input"
    },
    {
      "term": {
        "@type": "Term",
        "name": "Seed",
        "termType": "other",
        "@id": "seed",
        "units": "kg"
      },
      "@type": "Input"
    }
  ],
  "practices": [
    {
      "term": {
        "@type": "Term",
        "name": "Rotation duration",
        "termType": "landUseManagement",
        "@id": "rotationDuration",
        "units": "Days"
      },
      "description": "winter wheat, winter rye, intercropped barley-peas",
      "@type": "Practice"
    },
    {
      "term": {
        "@type": "Term",
        "name": "Minimum tillage",
        "termType": "tillage",
        "@id": "minimumTillage",
        "units": "% area"
      },
      "@type": "Practice"
    }
  ],
  "emissions": [
    {
      "term": {
        "@type": "Term",
        "name": "N2O, to air, soil flux",
        "termType": "emission",
        "@id": "n2OToAirSoilFlux",
        "units": "kg N2O"
      },
      "methodTier": "tier 2",
      "methodModel": {
        "@type": "Term",
        "name": "IPCC (2006)",
        "termType": "model",
        "@id": "ipcc2006"
      },
      "value": [
        2.1208030962747944
      ],
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "NOx, to air, soil flux",
        "termType": "emission",
        "@id": "noxToAirSoilFlux",
        "units": "kg NOx"
      },
      "methodTier": "tier 2",
      "methodModel": {
        "@type": "Term",
        "name": "Other model",
        "termType": "model",
        "@id": "otherModel"
      },
      "value": [
        0.44537010159651663
      ],
      "description": "SALCA model",
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "CH4, to air, enteric fermentation",
        "termType": "emission",
        "@id": "ch4ToAirEntericFermentation",
        "units": "kg CH4"
      },
      "methodTier": "tier 2",
      "methodModel": {
        "@type": "Term",
        "name": "IPCC (2006)",
        "termType": "model",
        "@id": "ipcc2006"
      },
      "value": [
        12.641993226898887
      ],
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "NO3, to groundwater, soil flux",
        "termType": "emission",
        "@id": "no3ToGroundwaterSoilFlux",
        "units": "kg NO3"
      },
      "methodTier": "tier 2",
      "methodModel": {
        "@type": "Term",
        "name": "Other model",
        "termType": "model",
        "@id": "otherModel"
      },
      "value": [
        371.5529753265602
      ],
      "description": "Richner et al (2011)",
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "PO43-, to groundwater, soil flux",
        "termType": "emission",
        "@id": "po43ToGroundwaterSoilFlux",
        "units": "kg PO43-"
      },
      "methodTier": "tier 2",
      "methodModel": {
        "@type": "Term",
        "name": "Other model",
        "termType": "model",
        "@id": "otherModel"
      },
      "value": [
        0.3434929850024189
      ],
      "description": "Prasuhn (2006)",
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "PO43-, to surface water, soil flux",
        "termType": "emission",
        "@id": "po43ToSurfaceWaterSoilFlux",
        "units": "kg PO43-"
      },
      "methodTier": "tier 2",
      "methodModel": {
        "@type": "Term",
        "name": "Other model",
        "termType": "model",
        "@id": "otherModel"
      },
      "value": [
        0.1383647798742138
      ],
      "description": "Prasuhn (2006)",
      "@type": "Emission"
    },
    {
      "term": {
        "@type": "Term",
        "name": "P, to surface water, soil flux",
        "termType": "emission",
        "@id": "pToSurfaceWaterSoilFlux",
        "units": "kg P"
      },
      "methodTier": "tier 2",
      "methodModel": {
        "@type": "Term",
        "name": "Other model",
        "termType": "model",
        "@id": "otherModel"
      },
      "value": [
        2.2738268021286885e-05
      ],
      "description": "Prasuhn (2006)",
      "@type": "Emission"
    }
  ],
  "cycleDuration": 365,
  "name": "Wheat, grain - France - 2008 - Farmer baker using animal labo...",
  "schemaVersion": "21.4.0",
  "originalId": "FR_AL_2008",
  "dataPrivate": false
}