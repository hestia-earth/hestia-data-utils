### Usage
### pip install seaborn
import argparse
from enum import Enum
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from hestia_earth.utils.lookup import download_lookup, get_table_value, column_name
from dotenv import load_dotenv


load_dotenv()


from hestia_earth.distribution.log import logger
from hestia_earth.distribution.utils import SIGMA_SCALER
from hestia_earth.distribution.utils.fao import (
    LOOKUP_YIELD, get_FAO_crop_name, is_nonempty_str, get_mean_std_per_country_per_product,
    get_fao_yield, get_fao_fertuse
)
from hestia_earth.distribution.prior_yield import get_prior


class PlotType(Enum):
    prior_per_country_per_product = 'prior_per_country_per_product'
    fao_per_country = 'fao_per_country'
    fao_per_product = 'fao_per_product'
    fao_per_country_per_product = 'fao_per_country_per_product'
    world_mu_signma = 'world_mu_signma'
    prior_per_country_per_fert = 'prior_per_country_per_fert'
    fao_per_country_per_fert = 'fao_per_country_per_fert'


parser = argparse.ArgumentParser(description='Plot Yield Priors figures')
parser.add_argument('--output-file', type=str, default='prior.png',
                    help='Filepath to save the result to as PNG')
parser.add_argument('--type', type=str,
                    default='prior_per_country_per_product',
                    choices=PlotType.__members__)
parser.add_argument('--nsamples', type=int,
                    default=1000,
                    help='Number of samples to generate in the distribution plot')
parser.add_argument('--country-id', type=str, required=True,
                    help='Region `@id` from Hestia glossary')
parser.add_argument('--product-id', type=str,
                    help='Product term `@id` from Hestia glossary')
args = parser.parse_args()


def _split_yields_str(data_element: str):
    if not is_nonempty_str(data_element):
        print('missing value or error in data')
        return None, -1
    else:
        yields = [r.split(":") for r in [r for r in data_element.split(";")]]

        if ['-'] in yields:
            yields.pop(yields.index(['-']))

        yields = np.array(yields).transpose()
        yields = yields.astype(np.int32)

        n_years = len(yields[0])
        return yields, n_years


def _create_df_fao():
    """
    Create a DataFrame to store all FAO crop yield data.
    This DataFrame can be used by plotting functions, especially with multiple subplots.

    Returns
    -------
    pd.DataFrame
        A DataFrame to store all FAO crop yield data, with index of country codes and column names of FAO crop terms.
    """
    lookup = download_lookup(LOOKUP_YIELD)
    fao_products = lookup.dtype.names[1:]

    df_fao = pd.DataFrame(index=lookup['termid'], columns=fao_products)

    for country_id in lookup['termid']:
        for product_name in fao_products:
            fao_yields = get_table_value(lookup, 'termid', country_id, column_name(product_name))
            df_fao.loc[country_id, product_name] = fao_yields
    return df_fao


def _calculate_worldwide_mean_sigma(product_id: str):
    """
    Calculate the means and sigmas for worldwide means and standard deviations of FAO yield for a specific product.

    Parameters
    ----------
    product_id: str
        Crop product term ID from Hestia glossary, e.g. 'wheatGrain'.

    Returns
    -------
    list of values:
        Means of worldwide means, std of worldwide means, mean of worldwide std, std of worldwide std.
    """
    df_fao = _create_df_fao()
    world_means = []
    world_sigmas = []
    for gadm_code, row in df_fao.iterrows():
        stats = get_mean_std_per_country_per_product(product_id, gadm_code, get_fao_yield)
        if None not in stats:
            world_means.append(stats[0])
            world_sigmas.append(stats[1])
    world_means = np.array(world_means)
    world_sigmas = np.array(world_sigmas)
    return [world_means.mean(), world_means.std(), world_sigmas.mean(), world_sigmas.std()]


def plot_fao_products_by_country_code(output_file: str, country_code: str):
    """
    Plot FAO yield records for all products for a specific contry or region,

    Parameters
    ----------
    output_file: str
        Output figure file name, e.g. fao_yield_all_products_GBR.png
    country_code : str
        GADM country code, e.g. 'GADM-GBR', or a region name, e.g. 'region-south-america'.

    Returns
    -------
    str or None
        File path of the output figure being saved, or None if no output file path given.
    """
    df_fao = _create_df_fao()
    country_name = ' '.join(country_code.split('-')[1:])
    row = df_fao.loc[country_code].dropna(how='all')

    sns.set()
    fig, axs = plt.subplots(len(row.keys())//8+1, 8, sharex=True, sharey=False, squeeze=True, figsize=(24, 24))
    axs = axs.flatten()
    fig.suptitle(f"{country_name}: {len(row.keys())} crop products Time Series")

    for i, colname in enumerate(row.keys()[1:]):
        yields, n_years = _split_yields_str(row[colname])

        if n_years > 0:
            axs[i].plot(yields[0], yields[1], alpha=0.5)
            axs[i].set_title(f'{colname}:{n_years} records')

    plt.savefig(output_file)


def plot_fao_products_by_product_id(output_file: str, prod_code: str):
    """
    Plot all FAO yield records for a specified product name for all contries and regions,

    Parameters
    ----------
    output_file: str
        Output figure file name, e.g. fao_yield_all_countries_wheat.png
    prod_code : str
        Product code from HESTIA glossary, e.g. 'wheatGrain'.
    """
    df_fao = _create_df_fao()
    prod_id = get_FAO_crop_name(prod_code).lower()
    col = pd.DataFrame(df_fao[prod_id].dropna(how='all'), columns=[prod_id])

    sns.set()
    fig, axs = plt.subplots(len(col)//8+1, 8, sharex=True, sharey=False, squeeze=True, figsize=(24, 24))
    axs = axs.flatten()
    fig.suptitle(f'{prod_id} production from {len(col)} countries or regions (Source: FAO; Unit: hg/ha)')

    for i, (idx, row) in enumerate(col.iterrows()):  # per country
        yields, n_years = _split_yields_str(row[prod_id])

        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
        if n_years > 1:
            if 'GADM' in idx:  # a country
                axs[i].plot(yields[0], yields[1], alpha=0.5, color=colors[0])
            else:  # a region
                axs[i].plot(yields[0], yields[1], alpha=0.8, color=colors[1])
            axs[i].set_title(f'{idx}:{n_years} records')

    plt.savefig(output_file)


def plot_fao_10yr_by_product(output_file: str, product_id: str, country_id: str, func):
    """
    Plots the FAO yield records for a specific country/region for a specific product,
    and it returns the record as an array.

    Parameters
    ----------
    output_file: str
        Output figure file name, e.g. fao_yield_10yr_gbr_wheat.png
    term_id: str
        Crop product term ID from HESTIA glossary, e.g. 'wheatGrain'.
    country_id : str
        GADM country code, e.g. 'GADM-GBR', or a region name, e.g. 'region-south-america'.
    func: function
        Name of the functions being used to get FAO time-series values, e.g. get_fao_yield, get_fao_fertuse
    """
    yields = func(country_id, product_id, n_years=70)

    yields10yr = yields[:, -min(10, len(yields[0])):]
    mean_all_records = int(yields[1].mean())
    mean_10yr = int(yields10yr[1].mean())

    gap = max(yields[0]) - min(yields[0]) + 1

    fao_colname = get_FAO_crop_name(product_id)

    sns.set()
    fig, axs = plt.subplots(1, 1, sharex=True, sharey=False, squeeze=True, figsize=(4, 4))
    fig.suptitle(f'Annual {fao_colname}, {country_id} (kg/ha)')

    if 'GADM' in country_id:  # a country
        axs.plot(yields[0], yields[1], alpha=0.5)
    else:  # a region
        axs.plot(yields[0], yields[1], alpha=0.5, color=next(axs._get_lines.prop_cycler))['color']
    axs.plot(yields10yr[0], yields10yr[1], '*')
    axs.set_title(f'{gap} yr mean: {mean_all_records}, {min(10,gap)} yr mean {mean_10yr}')

    plt.savefig(output_file)


def plot_mu_and_sigma_all_country(output_file: str, product_id: str):
    """
    Plot the means and standard deviations of FAO yield for a specific product for all countries and regions.

    Parameters
    ----------
    output_file: str
        Output figure file name, e.g. scatterplot_mu_sigma_wheat.png
    product_id: str
        Crop product term ID from HESTIA glossary, e.g. 'wheatGrain'.
    """
    sns.set()
    plt.title(product_id)
    plt.xlabel('Mu (average yield)')
    plt.ylabel('Sigma (of yield means)')

    df_fao = _create_df_fao()
    [mm, ms, sm, ss] = _calculate_worldwide_mean_sigma(product_id)

    for country_id, row in df_fao.iterrows():
        _mean, _sigma = get_prior(country_id, product_id)
        if None not in [_mean, _sigma]:
            if 'GADM' not in country_id:
                plt.scatter(_mean, _sigma, label=country_id.split('-')[1:])
                txt = plt.annotate(' '.join(country_id.split('-')[1:]), (_mean, _sigma))
                txt.set_fontsize(8)
            else:
                plt.scatter(_mean, _sigma, alpha=0.2, label=country_id.split('-')[1:])
                if (_mean > mm+2.5*ms) or (_sigma > sm+2.5*ss):
                    tx = plt.annotate(' '.join(country_id.split('-')[1:]), (_mean, _sigma))
                    tx.set_fontsize(8)

    plt.savefig(output_file)


def plot_prior_by_country_by_product(output_file: str, country_code: str, product_id: str, func, sample_size=1000):
    """
    Plots prior statistics (means, std and n_years) of FAO yield for one product for one country.

    Parameters
    ----------
    output_file: str
        Output figure file name, e.g. prior_GBR-wheat_sample100.png
    country_code : str
        GADM country code, e.g. 'GADM-GBR', or a region name, e.g. 'region-south-america'.
    product_id: str
        Crop product term ID from HESTIA glossary, e.g. 'wheatGrain'.
    func: function
        Name of the functions being used to get FAO time-series values, e.g. get_fao_yield, get_fao_fertuse
    sample_size: int
        Number of samples to generate.
        Default: 1000.
    """
    vals = get_mean_std_per_country_per_product(product_id, country_code, func)

    if None not in vals:
        sns.set()

        country_name = ' '.join(country_code.split('-')[1:])

        prior_rand = np.random.normal(loc=vals[0], scale=vals[1]*SIGMA_SCALER, size=sample_size)
        prior_rand = [max(p, 0) for p in prior_rand]
        sns.histplot(prior_rand, kde=True).set(title=f'Prior of {product_id} from {country_name}')

        plt.savefig(output_file)
    else:
        print('Error plotting, value is empty.')


PLOT_BY_TYPE = {
    PlotType.prior_per_country_per_product.value: lambda args: plot_prior_by_country_by_product(
        args.output_file, args.country_id, args.product_id, get_fao_yield, args.nsamples
    ),
    PlotType.prior_per_country_per_fert.value: lambda args: plot_prior_by_country_by_product(
        args.output_file, args.country_id, args.product_id, get_fao_fertuse, args.nsamples
    ),
    PlotType.fao_per_country.value: lambda args: plot_fao_products_by_country_code(args.output_file, args.country_id),
    PlotType.fao_per_product.value: lambda args: plot_fao_products_by_product_id(args.output_file, args.product_id),
    PlotType.fao_per_country_per_product.value: lambda args: plot_fao_10yr_by_product(
        args.output_file, args.product_id, args.country_id, get_fao_yield
    ),
    PlotType.fao_per_country_per_fert.value: lambda args: plot_fao_10yr_by_product(
        args.output_file, args.product_id, args.country_id, get_fao_fertuse
    ),
    PlotType.world_mu_signma.value: lambda args: plot_mu_and_sigma_all_country(args.output_file, args.product_id),
    None: lambda args: logger.error('Unsupported plotting option.')
}


def main():
    PLOT_BY_TYPE.get(args.type)(args)
    logger.info(f'Figure saved as {args.output_file}')


if __name__ == "__main__":
    main()
