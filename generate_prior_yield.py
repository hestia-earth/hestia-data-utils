import argparse
import time
from dotenv import load_dotenv


load_dotenv()


from hestia_earth.distribution.prior_yield import generate_prior_yield_file


parser = argparse.ArgumentParser(description='Generating Yield Priors File')
parser.add_argument('--overwrite', action='store_true',
                    help='Whether to overwrite existing file')
args = parser.parse_args()


def main():
    start_time = time.time()
    generate_prior_yield_file(overwrite=args.overwrite)
    print(f"Finished. Total time: {int(time.time() - start_time)} secs.")


if __name__ == "__main__":
    main()
